﻿using System;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string[] dataArray; // Масив для зберігання даних з файлу
        private int dataIndex = 0; // Індекс для відстеження поточного рядка даних
        private DispatcherTimer timer; // Таймер для оновлення вмісту вікна
        private DateTime lastModifiedTime; // Перемінна для збереження часу останньої модифікації файла
        private Mutex mutex = new Mutex();

        public MainWindow()
        {
            InitializeComponent();

            // Ініціалізація таймера
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(500); // Інтервал таймера (0.5 секунди)
            timer.Tick += new EventHandler(UpdateDisplay);

            // Отримання часу останньої модифікації файла
            lastModifiedTime = File.GetLastWriteTime(@"C:\Users\pavlo\source\repos\sysprog1-main\generator\bin\Debug\net6.0\data.dat");

            // Початкове відображення даних
            dataArray = File.ReadAllLines(@"C:\Users\pavlo\source\repos\sysprog1-main\generator\bin\Debug\net6.0\data.dat"); // Додано ініціалізацію dataArray
            UpdateDisplay(null, null);

            // Запуск таймера
            timer.Start();
        }

        private void UpdateDisplay(object sender, EventArgs e)
        {
            // Перевірка, чи файл був змінений
            DateTime currentModifiedTime = File.GetLastWriteTime(@"C:\Users\pavlo\source\repos\sysprog1-main\generator\bin\Debug\net6.0\data.dat");

            if (currentModifiedTime != lastModifiedTime)
            {
                mutex.WaitOne(); // Захоплюємо мютекс перед оновленням даних з файлу

                // Оновлюємо дані з файла тут і відображаємо їх у вікні

                mutex.ReleaseMutex(); // Звільняємо мютекс після завершення роботи з файлом
                lastModifiedTime = currentModifiedTime;
            }

            // Перевірка, чи індекс в межах масиву
            if (dataIndex < dataArray.Length)
            {
                string dataLine = dataArray[dataIndex];
                int dataValue;

                // Спроба конвертувати рядок у число
                if (int.TryParse(dataLine, out dataValue))
                {
                    // Відображення числа у вигляді '*' в кількості dataValue
                    displayTextBlock.Text = new string('*', dataValue);
                }
                else
                {
                    // Якщо не вдалося конвертувати, відобразити повідомлення про помилку
                    displayTextBlock.Text = "Invalid Data";
                }

                // Збільшення індексу для наступного рядка
                dataIndex++;
            }
            else
            {
                // Якщо всі рядки виведено, зупинити таймер
                timer.Stop();
                displayTextBlock.Text = "End of Data";
            }
        }
    }
}
