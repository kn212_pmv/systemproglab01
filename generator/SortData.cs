﻿using System;
using System.IO;
using System.Threading;

namespace generator
{
    internal class SortData
    {
        private static Mutex mutex = new Mutex();

        static void Main(string[] args)
        {
            Console.WriteLine("Програма сортування даних. Натисніть пробіл, щоб почати сортування.");

            // Очікуємо натискання пробілу
            while (Console.ReadKey().Key != ConsoleKey.Spacebar) { }

            // Завантажуємо дані з файлу в масив
            int[] data;
            try
            {
                // Захоплюємо мютекс перед зчитуванням даних з файлу
                mutex.WaitOne();

                string[] lines = File.ReadAllLines("data.dat");
                data = new int[lines.Length];
                for (int i = 0; i < lines.Length; i++)
                {
                    if (int.TryParse(lines[i], out int number))
                    {
                        data[i] = number;
                    }
                    else
                    {
                        Console.WriteLine($"Помилка: Рядок {i + 1} не є числом.");
                        return;
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Помилка: Файл 'data.dat' не знайдено.");
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Помилка: {ex.Message}");
                return;
            }
            finally
            {
                // Звільняємо мютекс після завершення роботи з файлом
                mutex.ReleaseMutex();
            }

            // Виконуємо сортування
            Console.WriteLine("Початок сортування...");
            SortingData(data);
            Console.WriteLine("Робота завершена.");
        }

        static void SortingData(int[] data)
        {
            int n = data.Length;
            for (int i = 1; i < n; ++i)
            {
                int key = data[i];
                int j = i - 1;

                // Затримка для візуалізації сортування
                Thread.Sleep(1000);

                while (j >= 0 && data[j] > key)
                {
                    data[j + 1] = data[j];
                    j = j - 1;
                }
                data[j + 1] = key;

                // Запис поточного стану сортування у файл
                File.WriteAllLines("data.dat", data.Select(x => x.ToString()));
            }
        }
    }
}
