﻿using System;
using System.IO;

namespace dataGen
{
    class dataGen
    {
        static void Main()
        {
            // Генеруємо випадкові числа і записуємо їх у файл
            Random random = new Random();
            int count = random.Next(20, 31); // Випадкова кількість чисел від 20 до 30
            using (StreamWriter writer = new StreamWriter(@"C:\Users\pavlo\source\repos\sysprog1-main\generator\bin\Debug\net6.0\data.dat"))
            {
                for (int i = 0; i < count; i++)
                {
                    int randomNumber = random.Next(10, 101); // Випадкове число від 10 до 100
                    writer.WriteLine(randomNumber);
                }
            }

            Console.WriteLine($"Згенеровано та збережено {count} випадкових чисел у файлі 'data.dat'.");
            string currentDirectory = Directory.GetCurrentDirectory();

            Console.WriteLine($"Поточна робоча директорія: {currentDirectory}");

        }
    }
}